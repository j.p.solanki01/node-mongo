const promiseRouter = require('express-promise-router');
const router = promiseRouter();

const WebController = require('./../../controller/index').WebController;
router.get('/web/pizza', WebController.getPizzaList);

module.exports = router;
