const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const { authJWT } = require("./../../middleware");


const AdminController = require('./../../controller/index').AdminController;
router.post('/admin/add', AdminController.add);
router.post('/admin/login', AdminController.login);
router.post('/admin/pizza',[authJWT.verifyToken], AdminController.addPizza);
router.get('/admin/pizza',[authJWT.verifyToken], AdminController.getPizzaList);
router.put('/admin/pizza/:id',[authJWT.verifyToken], AdminController.updatePizza);
router.delete('/admin/pizza/:id',[authJWT.verifyToken], AdminController.removePizza);

module.exports = router;