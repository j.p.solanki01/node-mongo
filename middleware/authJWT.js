const jwt = require("jsonwebtoken");
const Encrypt = require('./../config/encrypt').CONFIG

const verifyToken = (req, res, next) => {
    let token = req.headers["authorization"];

    if (!token) {
    res.statusCode = 403;
      return res.json({
        message: 'No token provided!'
      });
    }
  
    jwt.verify(token, Encrypt.jwt_encryption, (err, decoded) => {
      if (err) {

        res.statusCode = 401;
        return res.json({
          message: 'Unauthorized'
        });
      }
      req.userId = decoded._id;
      next();
    });
  };

  module.exports = {verifyToken}
