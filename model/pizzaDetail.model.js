const mongoose = require('mongoose');
const pizzaDetailSchema = new mongoose.Schema({
    Name: {
		type: String,
		required: true
    },
    content: {
		type: String,
		required: true
    },
    price: {
		type: Number,
		required: true,
	},
},{ timestamps: true });

module.exports = mongoose.model('pizzaDetail', pizzaDetailSchema);
