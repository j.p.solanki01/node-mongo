const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const adminSchema = new mongoose.Schema({
    Name: {
		type: String,
		required: true
    },
    userName: {
		type: String,
		required: true
    },
    password: {
		type: String,
		required: true,
	},
});

adminSchema.methods.comparePassword = function(Password) {
   return bcrypt.compareSync(Password, this.password, function(err, isMatch) {
        if (err) return err;

        console.log(isMatch,"isMatch")
        return isMatch
    });
};

adminSchema.pre('save', function ()  {
	this.password = bcrypt.hashSync(this.password, 10);
  });

module.exports = mongoose.model('Admin', adminSchema);
