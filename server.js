const express = require('express');
const bodyParser= require('body-parser')
const promiseRouter = require('express-promise-router');
const router = promiseRouter();
const v1 = require('./routes/index');
const mongoose = require('./config/mongoose');
// app.get('/', (req, res) => {
//     res.send('testing')
//   })
const app = express()
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json())
  .use(router)

app.use('/api', v1);
app.listen(3000, function() {
    console.log('listening on 3000')
  })