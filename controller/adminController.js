const { of } = require('await-of');
const jwt = require('jsonwebtoken');
const Encrypt = require('./../config/encrypt').CONFIG
const Admin = require('./../model/admin.model');
const pizzaDetail = require('./../model/pizzaDetail.model');

 const add = async (req, res) => {
    const data = {
      ...req.body
    }
  	const [result, err] = await of(new Admin(data).save());
    console.log(result,"result")
    if (err) {
      console.log(err, "errr")
      res.statusCode = 400;
      return res.json({
        message: 'failed'
      });
    }
    res.statusCode = 201;
      return res.json({
        message: 'success'
      });
  }

 const login = async (req, res) => {
		try {
      const { userName, password } = req.body;
      if(!userName){
        res.statusCode = 400;
        return res.json({
        message: 'UserName required'
      });
      }
      if(!password){
        res.statusCode = 400;
        return res.json({
        message: 'Password required'
      });
      }
			const [admin, error] = await of(Admin.findOne({ userName }));
			if (error) {
				res.statusCode = 400;
        return res.json({
        message: 'failed'
      });
			}
			if (!admin) {
        res.statusCode = 400;
        return res.json({
          message: 'Invalid userName'
        });
      }

			if (!admin.comparePassword(password)) {
				res.statusCode = 400;
        return res.json({
          message: 'Invalid password'
        });
			} else {

        console.log('sucessful')
        let token = jwt.sign({ id: admin._id }, Encrypt.jwt_encryption, {
          expiresIn: 86400 // 24 hours
        });  

        res.setHeader('Content-Type', 'application/json');
        res.setHeader('AuthToken', token);
        res.setHeader("Access-Control-Expose-Headers", "AuthToken");// //Authorization

        res.statusCode = 200;
        return res.json({
          message: 'Logged-in successfully'
        });
      }
		}
		catch (err) {
			res.statusCode = 400;
        return res.json({
          message: err
        });
		}
  } 
  
  const addPizza = async (req, res) => {
    const data = {
      ...req.body
    }
  	const [result, err] = await of(new pizzaDetail(data).save());
    if (err) {
      console.log(err, "errr")
      res.statusCode = 400;
      return res.json({
        message: 'failed'
      });
    }
    res.statusCode = 201;
      return res.json({
        message: 'success'
      });
  } 

  const getPizzaList = async (req, res) => {
    const [pizzaData, err] = await of(pizzaDetail.find().sort({ createdAt: -1 }))
    if (err){
      res.statusCode = 400;
      return res.json({
      message: 'failed'
      });
    }
    res.statusCode = 200;
      return res.json({
        message: 'success',
        data:pizzaData
      });
  }

  const updatePizza = async (req, res) => {

    const [result, err] = await of(pizzaDetail.findByIdAndUpdate(req.params.id, req.body));
    if (err) {
      res.statusCode = 400;
      return res.json({
      message: 'failed'
      });
    }
    res.statusCode = 201;
      return res.json({
        message: 'success',
      });
  }

  const removePizza = async (req, res) => {
    const [result, err] = await of(pizzaDetail.findByIdAndDelete(req.params.id));
    if (err) {
      res.statusCode = 400;
      return res.json({
      message: 'failed'
      });
    }
    res.statusCode = 200;
      return res.json({
        message: 'success',
      });
  }

  module.exports = {
    add,
    login,
    addPizza,
    getPizzaList,
    updatePizza,
    removePizza
}