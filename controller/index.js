const AdminController = require('./adminController');
const WebController = require('./webController');

module.exports = {
    AdminController,
    WebController
}