const { of } = require('await-of');
const pizzaDetail = require('./../model/pizzaDetail.model');

const getPizzaList = async (req, res) => {
    let filter = {};
    if(req.query.price != ''){
        filter = { "price": { "$lte":req.query.price } };
    }
    const [pizzaData, err] = await of(pizzaDetail.find(filter).sort({ createdAt: -1 }))
    if (err){
      res.statusCode = 400;
      return res.json({
      message: 'failed'
      });
    }
    res.statusCode = 200;
      return res.json({
        message: 'success',
        data:pizzaData
      });
  }
module.exports = {
    getPizzaList
}  